<?php

use Illuminate\Database\Seeder;

class RecipesHaveIngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Creates a link between recipes and ingredients
     *
     * @return void
     */
    public function run()
    {
        DB::table('recipe_ingredient')->insert([
            'recipe_id' => '1',
            'ingredient_id' => '1',
            'amount' => '100',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('recipe_ingredient')->insert([
            'recipe_id' => '1',
            'ingredient_id' => '2',
            'amount' => '50',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('recipe_ingredient')->insert([
            'recipe_id' => '1',
            'ingredient_id' => '3',
            'amount' => '60',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('recipe_ingredient')->insert([
            'recipe_id' => '2',
            'ingredient_id' => '4',
            'amount' => '300',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('recipe_ingredient')->insert([
            'recipe_id' => '2',
            'ingredient_id' => '5',
            'amount' => '25',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('recipe_ingredient')->insert([
            'recipe_id' => '2',
            'ingredient_id' => '6',
            'amount' => '2500',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);
    }
}
