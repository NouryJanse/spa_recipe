<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(IngredientSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(StepSeeder::class);
        $this->call(RecipeSeeder::class);
        $this->call(RecipesHaveIngredientsSeeder::class);
    }
}
