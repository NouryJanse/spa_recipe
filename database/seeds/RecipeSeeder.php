<?php

use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recipes')->insert([
        	'title' => 'Broodje kip',
            'user_id' => '1',
        	'description' => 'Dit broodje met pittige kipstukjes, komkommer en sla is ideaal als snelle maaltijd. Snijd de ui in kleine stukjes en bak totdat ze glazig zijn. Snijd de kip en paprika in stukjes en de rode peper en knoflook heel fijn. Voeg dit samen met de honing, mosterd, knoflook en paprikapoeder toe aan de pan.',
            'image' => ('chicken.png'),
            'duration' => '3600',
            'total_calories' => '2450',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
    	]);
    	DB::table('recipes')->insert([
        	'title' => 'Pittige indische rundvleesstoofpot ',
            'user_id' => '2',
        	'description' => 'Een heerlijke Indische stoofpot. Een top-recept! Lekker als onderdeel van een rijsttafel, maar ook heel erg lekker als hoofdgerecht.',
            'image' => ('rice.jpg'),
            'duration' => '1200',
            'total_calories' => '1337',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')            
    	]);
    }
}
