<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Noury',
        	'email' => 'hello@nouryjanse.nl',
        	'password' => bcrypt('secret'),
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis molestie ligula, eu porta elit. Aliquam non tellus et lectus aliquet elementum. Fusce eu odio nec orci consequat eleifend eleifend non mi. Morbi non erat euismod, molestie mi id, pretium nibh. Curabitur enim orci, pellentesque eget erat nec, suscipit commodo tortor. Nulla non tristique diam. Donec vitae quam blandit, rhoncus elit id, egestas turpis.',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')             
        ]); 
        DB::table('users')->insert([
            'name' => 'Pete',
            'email' => 'hi@nouryjanse.nl',
            'password' => bcrypt('secret2'),
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis molestie ligula, eu porta elit. Aliquam non tellus et lectus aliquet elementum. Fusce eu odio nec orci consequat eleifend eleifend non mi. Morbi non erat euismod, molestie mi id, pretium nibh. Curabitur enim orci, pellentesque eget erat nec, suscipit commodo tortor. Nulla non tristique diam. Donec vitae quam blandit, rhoncus elit id, egestas turpis.',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')             
        ]);
    }
}
