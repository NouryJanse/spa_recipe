<?php

use Illuminate\Database\Seeder;

class StepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('steps')->insert([
            'recipe_id' => '1',
            'stepnumber' => '1',
            'description' => 'Kip in de water',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')               
        ]);
        DB::table('steps')->insert([
            'recipe_id' => '1',
            'stepnumber' => '2',
            'description' => 'Hete kip',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')               
        ]);
        DB::table('steps')->insert([
            'recipe_id' => '1',
            'stepnumber' => '3',
            'description' => 'Broodje kip',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')               
        ]);
        DB::table('steps')->insert([
            'recipe_id' => '2',
            'stepnumber' => '1',
            'description' => 'Goeie koe',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')               
        ]);
        DB::table('steps')->insert([
            'recipe_id' => '2',
            'stepnumber' => '2',
            'description' => 'Gefrituurde koe',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')               
    	]);

    }
}
