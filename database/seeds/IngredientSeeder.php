<?php

use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredients')->insert([
            'name' => 'Kip',
            'description' => 'Malse naturel kipfilet om eindeloos mee te variëren. Lekker met wat kruiden en marinade en makkelijk te snijden in blokjes, reepjes, plakjes etc.',
            'calories' => '1.1',
            'unit' => 'gr',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Tij­ger­brood wit half',
            'description' => 'Lekker zacht witbrood met een mooie gemêleerde korst. Om naar eigen smaak te beleggen en ideaal voor het maken van een lekkere tosti.',
            'calories' => '2.25',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]); 
        DB::table('ingredients')->insert([
            'name' => 'Bi­o­lo­gisch Vol­ko­ren spa­ghet­ti',
            'description' => 'Spaghetti met een pure smaak. Bron van vezels. Ook geschikt voor het maken van pastataarten.',
            'calories' => '3.4',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);          
        DB::table('ingredients')->insert([
            'name' => 'Ge­rasp­te kaas voor pas­ta',
            'description' => 'Speciaal voor Pasta, een melange van maasdammer en Goudse jong belegen kaas.',
            'calories' => '3.5',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);         
        DB::table('ingredients')->insert([
            'name' => 'To­ma­ten',
            'description' => 'Tomaten zijn sappig en vol van smaak. Een gezonde aanvulling in de salade of voor op de boterham.',
            'calories' => '0.01',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('ingredients')->insert([
            'name' => 'Mager rundergehakt',
            'description' => 'Mager rundergehakt met minder vet om eindeloos mee te variëren. Natuurlijk voor een gewone gahaktbal, maar ook voor soepballetjes, of gebruik het geruld door spaghetti bolognese.',
            'calories' => '1.9',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('ingredients')->insert([
            'name' => 'Rijst',
            'description' => 'Pandanrijst is een langkorrelrijst met een bijzondere geur en smaak uit Zuidoost-Azië. Pandanrijst heeft een mooie, lange, witte korrel en blijft na het koken licht vochtig.',
            'calories' => '3.5',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('ingredients')->insert([
            'name' => 'Melk',
            'description' => 'Verse halfvolle melk voor iedere dag. Een natuurlijke bron van calcium met het groene vinkje van het Voedingscentrum, oftewel een gezondere keuze!',
            'calories' => '0.48',
            'unit' => 'ml',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Komkommer',
            'description' => 'Komkommer heeft een frisse, knapperige, neutrale smaak. Heerlijk door een salade of als gezonde snack.',
            'calories' => '0.01',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('ingredients')->insert([
            'name' => 'Frambozen',
            'description' => 'Frambozen hebben en fluweelzachte en zoete smaak. Het is beter om de frambozen niet te wassen, zo blijft de heerlijke smaak het beste bewaard. Lekker door een fruitsalade of smoothie.',
            'calories' => '0.35',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')         
        ]);        
        DB::table('ingredients')->insert([
            'name' => 'Aardbeien',
        	'description' => 'Aardbeien zijn al eeuwen de meest geprezen zachte fruitsoort. Ze worden ook wel zomerkoninkjes genoemd. De aardbei is eigenlijk geen vrucht maar een schijnvrucht, de pitjes aan de buitenkant zijn de eigenlijke vruchtjes. Aardbeien zijn er in vele variëteiten waarbij grootte en kleur niets zeggen over de kwaliteit.',
            'calories' => '0.35',
            'unit' => 'gr',            
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')        	
    	]);
    }
}
