<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'recipe_id' => '1',
        	'user_id' => '1',
        	'body' => 'Tastes great!',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);           
        DB::table('comments')->insert([
        	'recipe_id' => '1',
            'user_id' => '2',
        	'body' => 'Looks good.',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);        
        DB::table('comments')->insert([
        	'recipe_id' => '2',
            'user_id' => '1',
        	'body' => 'What a fantastic dish.',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);        
        DB::table('comments')->insert([
        	'recipe_id' => '2',
            'user_id' => '2',
        	'body' => 'Want to cook it today :).',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
