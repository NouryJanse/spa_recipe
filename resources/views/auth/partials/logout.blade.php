@if ( Auth::check() )
	<a href="{{ url('/logout') }}"
	    onclick="event.preventDefault();
	             document.getElementById('logout-form').submit();" class="nav-item">
	    Logout
	</a>

	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	    {{ csrf_field() }}
	</form>
@endif