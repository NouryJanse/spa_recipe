@extends('layouts.master')

@section ('content')

    <div class="container">
        <div class="columns">
            <div class="column is-3"></div>
            <div class="column is-9">    
            	You are not authorised to access this page. Please go back to the homepage.
            <div class="column is-0"></div>
        </div>
    </div>

@endsection