@extends('layouts.master_login')

@section('content')

    <div class="login-page">
        <div class="form">

            <form class="login-form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                        <input id="email" type="email" class="form-control" name="email" placeholder="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button type="submit">
                        Send Password Reset Link
                    </button>

                </form>

                {{--  <p class="message">Not registered? <a href="/register">Create an account</a></p>  --}}
                {{--  <p class="message"><a href="{{ route('password.request') }}">Forgot Your Password?</a></p>  --}}

            </form>
        </div>
    </div>
@endsection