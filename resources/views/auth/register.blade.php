@extends('layouts.master_login')

@section('content')

    <div class="login-page">
        <div class="form">

            <div class="field has-addons">
                <p class="control">
                    <a class="button" href="/login">
                        <span>Login</span>
                    </a>
                </p>         
                <p class="control">
                    <a class="button active" href="/register">
                        <span>Register</span>
                    </a>
                </p>
            </div>          

            <form class="login-form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" placeholder="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" placeholder="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" placeholder="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="password confirmation" required>
                    </div>
                </div>

                <button>Register</button>
                <p class="message">Not registered? <a href="/register">Create an account</a></p>
                <p class="message"><a href="{{ route('password.request') }}">Forgot Your Password?</a></p>

            </form>
        </div>
    </div>

@endsection