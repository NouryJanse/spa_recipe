<header class="hero is-primary is-small">
  <!-- Hero header: will stick at the top -->
  <div class="hero-head">
    <header class="nav">
      <div class="container">
        <div class="nav-left">
          <a class="nav-item">
            Recipes by Noury
          </a>
        </div>
        <span class="nav-toggle" onclick="document.querySelector('.nav-menu').classList.toggle('is-active');">
          <span></span>
          <span></span>
          <span></span>
        </span>
        
        <div class="nav-right nav-menu">
          
          @if ( !Auth::check() )
            <a class="nav-item is-active" href="/login">Login</a>
          @endif

          @include('auth.partials.logout')     

          @if ( !Auth::check() )
            <a class="nav-item" href="/register">Register</a>
          @endif

          @if ( Auth::check() )
            <a class="nav-item" href="/my/recipes">
              My Account
            </a>
          @endif
        </div>
      </div>
    </header>
  </div>

  <!-- Hero content: will be in the middle -->
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Empowering the Cook
      </h1>
      <h2 class="subtitle">
        by Noury
      </h2>
    </div>
  </div>

  <!-- Hero footer: will stick at the bottom -->
  <div class="hero-foot">
    @include('layouts.nav')
  </div>
</header>