<?php $currentRouteName = str_replace('.', '-', Route::currentRouteName()); ?>

<nav class="tabs is-boxed is-centered">
  <div class="container">
    <ul>
        <a class="nav-link {{$currentRouteName == 'home' ? 'is-active' : ''}}" href="/">Home</a>

        @if ( Auth::check() )
            <a class="nav-link {{$currentRouteName == 'create-recipe' ? 'is-active' : ''}}" href="/recipes/create">Create Recipe</a>
        @endif

        @if ( Auth::check() )
            <a class="nav-link {{$currentRouteName == 'my-recipes' ? 'is-active' : ''}}" href="/my/recipes/">My Recipes</a>
        @endif

    </ul>
  </div>
</nav>