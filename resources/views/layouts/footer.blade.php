<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        <strong>Recipes</strong> by <a href="http://nouryjanse.nl/">Noury Janse</a>
      </p>
      <p>
        <a class="icon" href="https://bitbucket.com/nouryjanse/">
          <i class="fa fa-bitbucket"></i>
        </a>
      </p>
    </div>
  </div>
</footer>