<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <link rel="stylesheet" href="/css/app.css">

        <script type="text/javascript">
            window.Laravel = { csrfToken: '{{ csrf_token() }}' };
            window.isAuthenticated = {{ $authenticated }};
            window.withEditRights = {{ $withEditRights }};
            window.userId = {{ $userId }};
        </script>

        <title>Recipes by Noury</title>
    </head>
    <body class="login-page-styling">

        <div id="app" class="login-master">
            <section class="body">
                <div class="container">
                    <div class="columns">
                        <div class="column is-4"></div>
                        <div class="column is-4">
                            @yield('content')
                        </div>
                        <div class="column is-4"></div>
                    </div>
                </div>
            </section>
        </div>

    </body>

    <script type="text/javascript" src="/js/app.js"></script>
    
</html>
