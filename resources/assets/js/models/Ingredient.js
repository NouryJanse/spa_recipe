class Ingredient {

    /**
     * Returns all ingredients via async axios
     */
    static all(then) {
        return axios.get('/api/ingredients/')
            .then(({ data }) => then(data));
    }


    /**
     * Returns 1 recipe by recipename via async axios
     */
    static findIngredientByName(then, ingredientName) {
    	console.log('/api/ingredients/'+ingredientName);
    	return axios.get('/api/ingredients/'+ingredientName)
    		.then(({data}) => then (data));
    }


    /**
     * Creates an ingredient
     */
    static create(ingredient) {
        // no implementation yet
    }
}

export default Ingredient;
