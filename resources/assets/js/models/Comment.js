class Comment {

    /**
     * Delete 1 recipe by recipeid via async axios
     */
    static delete(then, id) {
        return axios.get('/api/recipebyid/' + id)
            .then(({ data }) => then(data));
    }

    /**
     * Creates a recipe async axios
     */
    static create(recipe_id, comment) {
        console.log(recipe_id);
        var formData = new FormData();
        formData.append('recipe_id', recipe_id);
        formData.append('body', comment);

        return axios.post('/api/comments', formData)
            .then(function(data) {
                return data;
            })
            .catch(e => {
                return e;
            });
    }

}

export default Comment;