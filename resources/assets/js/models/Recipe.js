class Recipe {
    /**
     * Returns all recipes via async axios
     */
    static all(then) {
        return axios.get('/api/recipes')
            .then(({ data }) => then(data));
    }


    /**
     * Returns 1 recipe by id via async axios
     */
    static find(then, id) {
        return axios.get('/api/recipes/' + id)
            .then(({ data }) => then(data));
    }

    /**
     * Creates a recipe async axios
     */
    static create(recipe) {
        var formData = new FormData();
        formData.append('image', recipe.image);
        formData.append('title', recipe.title);
        formData.append('description', recipe.description);
        formData.append('duration', recipe.duration);
        formData.append('ingredients', JSON.stringify(recipe.ingredients));
        formData.append('steps', JSON.stringify(recipe.steps));

        return axios.post('/api/recipes', formData)
            .then(function(data) {
                return data;
            })
            .catch(e => {
                return e;
            });
    }

    /**
     * Update a recipe async axios
     */
    static update(recipe) {
        var formData = new FormData();
        formData.append('image', recipe.image);
        formData.append('title', recipe.title);
        formData.append('description', recipe.description);
        formData.append('duration', recipe.duration);
        formData.append('ingredients', JSON.stringify(recipe.ingredients));
        formData.append('steps', JSON.stringify(recipe.steps));

        formData.append('_method', 'PATCH');

        return axios.post('/api/recipes/' + recipe.id, formData, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
            .then(function(data) {
                return data;
            })
            .catch(e => {
                return e;
            });
    }

    /**
     * Deletes a recipe
     */
    static destroy(recipe) {
        return axios.delete('/api/recipes/' + recipe.id)
            .then(function(data) {
                return data;
            })
            .catch(e => {
                return e;
            });
    }

}

export default Recipe;