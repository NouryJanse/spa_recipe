class Profile {

    /**
     * Retrieves the user profile
     */
    static find(then, userId) {
        return axios.get('/api/profile/' + userId)
            .then(({ data }) => then(data))
            .catch(e => {
                return e.response;
            });
    }
}

export default Profile;