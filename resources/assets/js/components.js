/**
 * 
 *	This file declares all Vue components in order to be included easily for later use.
 *
 */


/**
 * Homepage Vue Component
 */
Vue.component('home', require('./views/Home/home.vue').default);


/**
 * Create Recipe Vue Component
 */
Vue.component('create-recipe', require('./views/Recipe/create.vue').default);


/**
 * Show Recipe Details Vue Component
 */
Vue.component('show-recipe', require('./views/Recipe/show.vue').default);


/**
 * Create Comment Vue Component
 */
Vue.component('my-recipes', require('./views/My/recipes.vue').default);