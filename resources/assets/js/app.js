/**
 * 
 *	This document requests all necessary components for the app and initializes the Vue component
 *
 */

require('./bootstrap');
require('./components');

/**
 * Create a fresh Vue application instance 
 */
const app = new Vue({
    el: '#app'
});

Vue.directive('focus', {
    inserted: function(el, binding, vnode) {
        Vue.nextTick(function() {
            el.focus()
        })
    }
})