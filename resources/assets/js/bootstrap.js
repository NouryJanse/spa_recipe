/**
 * 
 *	This document bootstraps all neccesary files and components for the project + CSRF configuration.
 *
 */

/**
 * Import both Vue.js and Axios (AJAX Requests)
 */
window.Vue = require('vue');
window.axios = require('axios');


/**
 * Place CSRF Tokens for safe form submissions
 */
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


/**
 * Import the component MultiSelect (custom input field)
 */
import Multiselect from 'vue-multiselect';
Vue.component(Multiselect);


/**
 * Import the component Vuelidate - (Vue validation plugin)
 */
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);


import Toasted from 'vue-toasted';
Vue.use(Toasted);


Vue.use(require('vue-cookies'));