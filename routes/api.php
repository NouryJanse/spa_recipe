<?php

use Illuminate\Http\Request;
use App\User;


/**
 * Generates all default routes for the API
 * index, store, create, show, update, destroy, edit
 */

Route::group(['middleware' => 'auth:api', 'as' => 'auth_reqd.'], function () {

	Route::middleware('auth:api')->get('/user', function (Request $request) {
    	return User::with('roles')->get()->find($request->user()->id);
	});

	Route::resource('recipes', 'Api\RecipesController');
	Route::resource('ingredients', 'Api\IngredientsController');
	Route::resource('comments', 'Api\CommentsController');
});

// excluding index from the required authorization 
Route::get('recipes', 'Api\RecipesController@index');

// route for showing ONE recipe in detail without requiring authorization
Route::get('recipes/{recipe}', 'Api\RecipesController@show');

// not used
Route::middleware('auth:api')->get('profile/{profile}', 'Api\ProfileController@getUser');