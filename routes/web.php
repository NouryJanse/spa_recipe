<?php

Route::get('/', function () {
    return view('home');
});

/**
 * Generates all default routes for returning views
 * index, store, create, show, update, destroy, edit
 */

Auth::routes();

Route::resource('recipes', 'Routes\RecipesController');

Route::get('/home', 'Routes\HomeController@index')->name('home');
Route::get('/recipes/create/{recipe}', 'Routes\RecipesController@create');
Route::get('/my/recipes', 'Routes\ProfileController@show');