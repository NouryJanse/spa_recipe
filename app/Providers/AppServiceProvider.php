<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Auth;
use App\Recipe;
use App\User;
// use App\Http\Controllers\Api\ProfileController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* Enable the master and master login template to load with all Authentication data available */
        view()->composer(['layouts.master', 'layouts.master_login'], function($view) 
        {
            $this->bootstrapAuth($view);
        });
        }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /** 
     * Retrieves all available user data from the logged in user
     * Returns whether there is a user authenticated, the user and if it has edit rights
     */
    public function bootstrapAuth($view) {
        $id = Auth::id();
        $user = User::find($id);
        $isAuthenticated = Auth::check() ? 1 : 0;

        if($user = User::find($id)) {
            $withEditRights = $user->authorizeRoles('admin') ? 1 : 0;  
        } else {
            $withEditRights = 0;
        }

        $view->with('authenticated', $isAuthenticated);
        $view->with('withEditRights', $withEditRights);

        //@TODO: extra controles toevoegen
        $view->with('userId', Auth::id() ? Auth::id() : 0);
    }
}
