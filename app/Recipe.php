<?php

namespace App;
use Auth;

class Recipe extends Model
{
    // $recipe->ingredients
    public function ingredients() {
        return $this->belongsToMany(Ingredient::class, 'recipe_ingredient', 'recipe_id', 'ingredient_id')->withPivot('amount')->withTimestamps();
    }   

    // $recipe->comments
    public function comments() {
        return $this->hasMany(Comment::class);
    }    

    // $recipe->steps
    public function steps() {
        return $this->hasMany(Step::class);
    }    
    
    // $recipe->user->name;
    public function user() {
        return $this->belongsTo(User::class)
            ->select(['id', 'name']);
    }    

    // check whether the user is authorised to edit by matching the user id with the creator user id
    public static function isAuthorisedToEdit($id) {
        if(Auth::user()->id == Recipe::find($id)->user->id) {
            return true;
        } else {
            return false;
        }
    }    
}
