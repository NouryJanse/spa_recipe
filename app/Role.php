<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
		// $user->role
		public function users()
		{
				return $this->belongsToMany('App\User')->withTimestamps();
		}
}
