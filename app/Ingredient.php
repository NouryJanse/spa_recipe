<?php

namespace App;

class Ingredient extends Model
{
	protected $appends = ['amount'];

	// $ingredient->recipe
	public function recipe() {
		return $this->belongsToMany(Recipe::class);
	}

	// retrieve amount value from pivot and bind to the ingredient 
	public function getAmountAttribute()
	{
	   return ($this->pivot) ? $this->pivot->amount : null;
	}	
}
