<?php

namespace App;

class Comment extends Model
{
	protected $guarded = [];

	// $comment->recipe
	public function recipe() {
		return $this->belongsTo(Recipe::class);
	}

	// $comment->user->name;
	public function user() {
		return $this->belongsTo(User::class);
	}
}
