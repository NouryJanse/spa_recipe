<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Recipe;
use App\Ingredient;

class IngredientsController extends Controller
{

    /** 
     * @TODO: documentation
     */    
    public function index() {
        return Ingredient::all();
    }

    public function store($vals) {
    	$recipe = Recipe::find(1);

		$recipe->ingredients()->attach(request('1'), ['amount' => 100]);
		$recipe->ingredients()->attach(request('2'), ['amount' => 250]);
		$recipe->save();
        return $recipe;
    }

    public function show($ingredientName) {
        return Ingredient::where('name', $ingredientName)->get();
    }

    public function destroy() {
    	$recipe->ingredients()->detach(6);
    }

    public function update() {
    	$recipe->shops()->sync([1, 2, 3]);
    }
}
