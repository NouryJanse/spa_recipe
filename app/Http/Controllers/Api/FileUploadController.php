<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use \Illuminate\Http\UploadedFile;

class FileUploadController extends Controller
{

		/** 
		 * @TODO: documentation
		 */
    public static function storeFile($avatar) {
			$filename = time() . '.' . $avatar->getClientOriginalExtension();
			Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename) );

			return $filename;
    }
}
