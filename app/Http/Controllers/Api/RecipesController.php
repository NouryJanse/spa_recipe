<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Recipe;
use App\Ingredient;
use App\Step;
use Auth;
use App\Http\Controllers\Api\FileUploadController;


class RecipesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Recipe::with('user')->with('ingredients', 'comments', 'steps')->latest()->get();        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @todo: rework code and refactor with update method
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required|min:3',
            'description' => 'required',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $recipe = new Recipe;
        $recipe->user_id = Auth::id();
        $recipe->title = request('title');
        $recipe->description = request('description');
        $recipe->total_calories = 0;
        $recipe->duration = request('duration');
        $ingredients = json_decode(request('ingredients'));
        $steps = json_decode(request('steps'));

        // Check if image was presented, if so: store image and pass stored location
        if($request->hasFile('image')) {        
            $recipe->image = FileUploadController::storeFile($request->file('image'));
        }

        $recipe->save();

        // If ingredients are supplied, loop and attach them to the recipe
        foreach ($ingredients as $ingredient) {
            $recipe->total_calories += $ingredient->calories * $ingredient->amount;
            $recipe->ingredients()->attach($ingredient->id, ['amount' => $ingredient->amount]);
        }

        $recipe->save();

        // If steps are supplied, loop and attach them to the recipe
        foreach ($steps as $step) {
            $recipe->steps()->save(new Step([
                'recipe_id' => $recipe->id,
                'stepnumber' => $step->stepnumber,
                'description' => $step->description
            ]));
        }

        return Recipe::with('ingredients')->with('steps')->get()->find($recipe->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return Recipe::with('ingredients', 'comments.user', 'steps', 'user')->get()->find($id); 
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @todo: rework code and refactor with store method
     */
    public function update(Request $request, $id)
    {
        $recipe = Recipe::find($id);

        $recipe->fill(
            [
                'title' => $request->title,
                'description' => $request->description,
                'duration' => $request->duration
            ]
        );

        $recipe->save();

        $ingredients = json_decode(request('ingredients'));
        $steps = json_decode(request('steps'));


        // Check if image was presented, if so: store image and pass stored location
        if($request->hasFile('image')) {        
            $recipe->image = FileUploadController::storeFile($request->file('image'));
        }

        $recipe->save();

        // // If ingredients are supplied, loop and attach them to the recipe
        $recipe->ingredients()->detach();
        
        $recipe->total_calories = 0; // reset total calories and re-calculate the amount based on the supplied ingredientsq
        foreach ($ingredients as $ingredient) {
            $recipe->total_calories += $ingredient->calories * $ingredient->amount;
            $recipe->ingredients()->attach($ingredient->id, ['amount' => $ingredient->amount]);
        }
        $recipe->save();

        // // If steps are supplied, loop and attach them to the recipe
        $recipe->steps()->delete();

        foreach ($steps as $step) {
            $recipe->steps()->save(new Step([
                'recipe_id' => $recipe->id,
                'stepnumber' => $step->stepnumber,
                'description' => $step->description
            ]));
        }        
        $recipe->save();

        return Recipe::with('ingredients')->with('steps')->get()->find($recipe->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $recipe = Recipe::find($id);
        $recipe->ingredients()->detach();
        $recipe->steps()->delete();
        $recipe->comments()->delete();

        $recipe->delete();

        //@TODO: create return value
    }
}
