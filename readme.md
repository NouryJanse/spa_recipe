# README #

This Bitbucket repo contains a demo application made by web developer Noury Janse.
As a fan of good food I have always been using resources to find great recipes. So far my experience with websites or apps that contain recipes hasn't been as immersive as I wanted it to be. That's why I started to think to create an web app that would reflect my ideals/vision on how a recipes web app should work. This repo is a work in progress and is created to demonstrate my skills as a web developer.

Technical information is detailed below. The project can be cloned and run without a hassle.

### Skills ###
* OOP
* Clean coding
* MVC coding
* Refactoring
* JavaScript, PHP, HTML, SCSS
* Front-End, Back-end separation. Data is shared via API, routes are handled via Laravel (Spa/Hybrid)
* Modular file structure


### Used Techniques & Tools ###
* Uses Vue.JS
* Uses Laravel
* Uses SCSS/SASS
* Uses a MySQL Database
* Uses Webpack
* Uses Git Versioning (Bitbucket & Sourcetree)
* Uses Sublime Text 3
* Uses Terminal for managing the Laravel project
* Uses Valet (MAC OSX)
* Uses Node.js


### Installation ###
1. make sure you've got node installed (nodejs.org) and up to date (npm i -g npm)
2. make sure you've got a machine that's able to run Laravel projects (https://laravel.com/docs/5.5#installation)
3. make sure you've got a mysql database running
4. clone the project from https://bitbucket.org/NouryJanse/spa_recipe/
5. run npm install via terminal or shell
6. run composer update --no-scripts via terminal or shell
7. make sure .env file is configured + database file is set correctly
8. run php artisan key:generate via terminal or shell
9. run php artisan passport:install via terminal or shell
10. run php artisan migrate --seed via terminal or shell
11. run npm run watch via terminal or shell

*run php artisan migrate:refresh --seed via terminal or shell to re-seed the database*

### Screenshots ###
[ Login screen ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/login_screen.png)

[ Overview screen ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/overview_screen.png)

[ Create recipe 1 ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/create_recipe_1.png)

[ Create recipe 2 ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/create_recipe_2.png)

[ Edit existing recipe ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/edit_existing_recipe.png)

[ View detail recipe 1 ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/recipe_detail_1.png)

[ View detail recipe 2 ](https://bytebucket.org/NouryJanse/spa_recipe/raw/83aab9377a0913faec1ae590f786983a464a4803/screenshots/recipe_detail_2.png)